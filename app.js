var path = require("path");
var express = require("express");
var app = express();

var bodyParser = require("body-parser");
var session = require("express-session");
var cookieParser = require('cookie-parser');

const  PORT = "port";

app.use(cookieParser());
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

require('./routes.js')(app); // load our routes and pass in our app and fully configured passport

app.use(express.static(__dirname + "/public"));
app.use("/bower_components", express.static(__dirname + "/bower_components"));

app.listen(process.env.PORT , function(){
    console.info("App Server started on " + process.env.PORT);
});